// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
import { getAuth, GoogleAuthProvider } from 'firebase/auth';
import { getFirestore } from 'firebase/firestore';

const firebaseConfig = {
  apiKey: "AIzaSyBmIPW3GHjunbEX-GIu11AvxdgcZXQdRx0",
  authDomain: "trabajopractico3-7ab63.firebaseapp.com",
  projectId: "trabajopractico3-7ab63",
  storageBucket: "trabajopractico3-7ab63.appspot.com",
  messagingSenderId: "451177318060",
  appId: "1:451177318060:web:164ada33e0a80a8c6cc109",
  measurementId: "G-KR0TH390Q3"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
export const auth = getAuth(app);
export const googleProvider = new GoogleAuthProvider();
export const db = getFirestore(app); 
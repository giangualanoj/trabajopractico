const { dbconnection } = require('../config/database')
const express = require('express')
const UploadFile = require('../middlewares/multer')
const { getProducto, createProducto, updateProducto, deleteProducto, returnImagen } = require('../controllers/productControllers')
const UploadImageToGCS = require('../middlewares/googleCloud')



const router = express.Router()

router.get('/verProducto', getProducto)
router.post('/crearProducto', createProducto)
router.put('/:id', updateProducto)
router.delete('/:id', deleteProducto)

router.post('/uploadImagen', UploadFile(), async (req, res) => {
    console.log(req.file)
    try {
        const fileName = req.file.originalname;
        const imageBuffer = req.file.path;
        const bucketName = 'uadetrabajo';
        const contentType = req.file.mimetype;

        await UploadImageToGCS(fileName, imageBuffer, bucketName, contentType);

        res.send('Imagen subida exitosamente a Google Cloud Storage 😆');
    } catch (error) {
        console.error('soy el error en uploadImagen hl: ', error);
        // res.status(500).send('Error al cargar la imagen a Google Cloud Storage');
    }
})



module.exports = router


const { Storage } = require('@google-cloud/storage');
// const fs = require('fs');
const path = require('path');


const storage = new Storage({
    keyFilename: "./copy-web-apple-bac604e7356d.json",
    projectId: 'tu-proyecto-id',
});


const bucketName = 'uadetrabajo';
const bucket = storage.bucket(bucketName);

const localFolderPath = './uploads';

async function DownloadFilesFromBucket() {
    const [files] = await bucket.getFiles();
    
    for (const file of files) {
        const localFilePath = path.join(localFolderPath, file.name);
        const options = {
            destination: localFilePath,
        };
        
        await bucket.file(file.name).download(options);
        console.log(`Archivo ${file.name} descargado exitosamente.`);
    }
}

// downloadFilesFromBucket().catch(console.error);

module.exports = DownloadFilesFromBucket



// este codigo lo que hace es cargar los archivos que se encuentran en la carpeta "uploads" a Google Cloud Storage



// const bucketName = 'uadetrabajo';
// const uploadFolderPath = './uploads';

// // Obtén una referencia al bucket
// const bucket = storage.bucket(bucketName);

// // Lee todos los archivos en la carpeta "upload"
// fs.readdir(uploadFolderPath, (err, files) => {
//     if (err) {
//         console.error('Error al leer los archivos de la carpeta "upload":', err);
//         return;
//     }
//     console.log('Archivos encontrados en la carpeta "upload":', files);

//     // Carga cada archivo en Google Cloud Storage
//     files.forEach((file) => {
//         const localFilePath = path.join(uploadFolderPath, file);
//         const destination = path.join('uploads', file); // Ruta de destino en el bucket

//         bucket.upload(localFilePath, {
//             destination: destination,
//         }, (err, file) => {
//             if (err) {
//                 console.error(`Error al cargar el archivo ${file}:`, err);
//                 return;
//             }

//             console.log(`Archivo ${file} cargado exitosamente.`);
//         });
//     });
// });